package com.testprojects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int circles = 120;
        List<Card> deck = new ArrayList<>();
        List<Integer> teleportDeck = new ArrayList<>();

        buildDeck(deck);

        int playerCount = 320;
        int[] playerScoreTally = new int[playerCount];
        int[] playerPosition = new int[playerCount];
        int[] lastCardPlayer = new int[playerCount];
        int[] winningCard = new int[circles];
        int iterations = 10;
        int decksUsedUp = 0;
        int currentPlayer;
        int maxDecksUsedUp = 0;
        int totalCardsUsed = 0;
        int noCard = 0;

        for (int i = 0; i < iterations; i++) {
            Random random = new Random();
            // begin the logic
            Collections.shuffle(deck);
            //build board
            buildTeleportDeck(deck, teleportDeck);
            Collections.shuffle(teleportDeck);

            //reset...time to start playing the game.
            boolean victory = false;
            for (int k = 0 ; k < playerCount; k++) {
                playerPosition[k] = -1;
                lastCardPlayer[k] = -1;
            }
            currentPlayer = 0;
            int cardCount = 0;
            int decksUsedUpThisGame = 0;

            while (!victory) {
                int roll = random.nextInt(6) + 1;
                if (roll == 6) {
                    //teleport and grab card
                    playerPosition[currentPlayer] = teleportDeck.get(cardCount);
                    lastCardPlayer[currentPlayer] = teleportDeck.get(cardCount);
                    cardCount++;
                    totalCardsUsed++;
                    if (cardCount == teleportDeck.size()) {
                        decksUsedUp++;
                        decksUsedUpThisGame++;
                        cardCount = 0;
                        Collections.shuffle(teleportDeck);
                    }
                } else {
                    if (playerPosition[currentPlayer] + roll < circles) {
                        playerPosition[currentPlayer] += roll;
                        //victory!
                        if (playerPosition[currentPlayer] == circles - 1) {
                            victory = true;
                            if (lastCardPlayer[currentPlayer] == -1) {
                                noCard++;
                            } else {
                                winningCard[lastCardPlayer[currentPlayer]]++;
                            }
                            playerScoreTally[currentPlayer]++;
                        }
                    }
                }

                if (!victory) {
                    int position = playerPosition[currentPlayer];
                    // landed on someone
                    for (int j = 0; j < playerCount; j++) {
                        if (j != currentPlayer && playerPosition[j] == position) {
                            playerPosition[j] = -1;
                        }
                    }
                }

                currentPlayer++;
                currentPlayer %= playerCount;
            }

            if (decksUsedUpThisGame > maxDecksUsedUp) {
                maxDecksUsedUp = decksUsedUpThisGame;
            }
        }

        System.out.println("Results: ");
        System.out.println("Player Count: " + playerCount);
//        System.out.println("Decks Used Up Total: " + decksUsedUp);
//        System.out.println("Total Games: " + iterations);
        System.out.println("----------------------------");
        System.out.println("Avg decks used up: " + decksUsedUp/(1f * iterations));
        System.out.println("Max Decks Used Up: " + maxDecksUsedUp);
//        System.out.println("Total Cards Used: " + totalCardsUsed);
        System.out.println("Decks Used Up Fractional: " + (totalCardsUsed/(1f * teleportDeck.size()))/(1f * iterations));
        System.out.println("No Cards Won: " + noCard);
        System.out.println("Tophat %: " + winningCard[112]/(1f*iterations));
        System.out.println("Straw %: " + winningCard[106]/(1f*iterations));
        System.out.println(">> Wins:");
        for (int l = 0; l < playerCount; l++) {
            System.out.println("Player " + l + ": " + playerScoreTally[l]);
        }
    }

    public static void buildDeck(List<Card> deck) {
        Card last = new Card(false, false, false, false, true);
        Card first = new Card(true, false, false, false, false);
        Card secondToLast = new Card(false, false, false, true, false);
        Card second = new Card(false, true, false, false, false);
        Card third = new Card(false, false, true, false, false);
        Card special1 = new Card(false, true, false, false, true);

        for (int i = 0; i < 2; i++) {
            deck.add(last);
        }
        for (int i = 0; i < 2; i++) {
            deck.add(first);
        }
        for (int i = 0; i < 4; i++) {
            deck.add(secondToLast);
        }
        for (int i = 0; i < 5; i++) {
            deck.add(second);
        }
        deck.add(third);
        for (int i = 0; i < 2; i++) {
            deck.add(special1);
        }
    }

    public static void buildTeleportDeck(List<Card> deck, List<Integer> teleportDeck) {
        //board index
        teleportDeck.clear();

        int b = 0;
        for (int j = 0; j < 4; j++) {
            for (int k = 0; k < 4; k++) {
                Card c = deck.get(j * 4 + k);

                boolean backwards = false;
                if (j % 2 == 1) {
                    backwards = true;
                }

                for (int l = 0; l < 5; l++) {
                    if (c.dots[backwards ? 4 - l : l]) {
                        teleportDeck.add(b);
                    }
                    b++;
                }
            }
            b += 6;
        }
        //lemonade and top hat
        teleportDeck.add(112);
        teleportDeck.add(106);
    }
}
