package com.testprojects;

/**
 * Created by Nikko on 12/26/2016.
 */
class Card {
    boolean[] dots;

    public Card (boolean a, boolean b, boolean c, boolean d, boolean e) {
        dots = new boolean[5];
        dots[0] = a; dots[1] = b; dots[2] = c; dots[3] = d; dots[4] = e;
    }
}